# Repository has been moved
This repository has been moved to [https://git.ugfx.io/ugfx/ugfx.git](https://git.ugfx.io/ugfx/ugfx.git).

Please use the **new repository**, this one has been removed permanently.

![image_moved](http://paste.ugfx.org/sores/7505d64a54e0/40fc41bc2c65.jpg)